﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities;

namespace LA1JAntennaAnalyzer
{
    /// <summary>
    /// The standard K6BEZ DAQ we've built and love.
    /// </summary>
    class K6BEZ_DAQ : IDataAcquisition, IDisposable
    {
        ILineCom comm;
        bool stop = false;

        public GraphInfo[] GraphInfo { get; private set; }

        public K6BEZ_DAQ(ILineCom comm)
        {
            this.comm = comm;
            GraphInfo = new GraphInfo[6];
            GraphInfo[0] = new GraphInfo("Frequency [MHz]", OxyColor.FromRgb(0, 0, 0), false, true);
            GraphInfo[1] = new GraphInfo("SWR", OxyColor.FromRgb(0, 0, 192), false, true);
            GraphInfo[2] = new GraphInfo("Forward", OxyColor.FromRgb(128, 128, 0), true, false);
            GraphInfo[3] = new GraphInfo("Reverse", OxyColor.FromRgb(0, 128, 128), true, false);
            GraphInfo[4] = new GraphInfo("Memory", OxyColor.FromRgb(192, 0, 0), false, true);
            GraphInfo[5] = new GraphInfo("Limit", OxyColor.FromRgb(128, 128, 128), false, true);
        }

        public void Dispose()
        {
            ((IDisposable)comm).Dispose();
        }

        List<double[]> IDataAcquisition.Sweep(int startFrequency, int stopFrequency, int steps)
        {
            List<double[]> ret = new List<double[]>();

            startFrequency = Math.Max(0, startFrequency);
            comm.Write(startFrequency + "A");
            Thread.Sleep(100);
            comm.Write(stopFrequency + "B");
            Thread.Sleep(100);
            comm.Write(steps + "N");
            Thread.Sleep(100);
            comm.Write("S");
            string line = "";
            DateTime starttime = DateTime.Now;
            Debug.WriteLine(ToString() + ": Scanning from " + (startFrequency / 1e6) + " to " + (stopFrequency / 1e6) + " MHz with " + steps + " steps. ");
            do
            {
                line = comm.Read();
                if (line != null)
                {
                    Debug.WriteLine("K6BEZ: RX: " + line);
                    try
                    {
                        string[] param = line.Split(',');
                        if (param.Length > 3)
                        {
                            double[] meas = new double[6];
                            double fwd = double.Parse(param[2], CultureInfo.InvariantCulture);
                            double rev = double.Parse(param[3], CultureInfo.InvariantCulture);
                            meas[0] = double.Parse(param[0], CultureInfo.InvariantCulture) / 1e6;
                            meas[1] = (fwd + rev) / (fwd - rev);
                            meas[2] = fwd;
                            meas[3] = rev;
                            meas[4] = double.NaN;
                            meas[5] = double.NaN;
                            ret.Add(meas);
                        }
                    } catch {

                    }
                }
                else
                {
                    line = "End";
                }
                if (this.stop)
                {
                    ret = null;
                    break;
                }
            } while (!line.Contains("End"));
            Debug.WriteLine(ToString() + ": Sweep done in " + (DateTime.Now - starttime).TotalSeconds + " seconds.");
            if (ret.Count == 0) return null;
            else return ret;
        }

        public bool Connect()
        {
            return comm.Connect();
        }

        public bool IsConnected()
        {
            bool done = false;
            comm.Write("?");
            string line;
            while (!done)
            {
                line = comm.Read();
                if (line != null)
                {
                    if (line.StartsWith("Num Steps", StringComparison.Ordinal))
                    {
                        return true;
                    }
                }
                else
                {
                    done = true;
                }
                Thread.Sleep(10);
            }
            return false;
        }

        public void Disconnect()
        {
            comm.Disconnect();
        }

        public override string ToString()
        {
            return "K6BEZ (text) @ " + comm.ToString();
        }

    }
}
