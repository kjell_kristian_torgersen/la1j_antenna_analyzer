﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using OxyPlot.Axes;
using System.IO.Ports;
using System.Diagnostics;
using System.Globalization;
using Utilities;
using System.Linq;
using Model;

namespace GUI {
    public partial class LA1JAntennaAnalyzerForm : Form, IView {
        ConfigFile configFile;
        IController Controller;
        
        bool connected = false;
        PlotHandler plotHandler;
        double[][] Values;
        GraphInfo[] GraphInfo;
        SweepParameters sp = new SweepParameters(1000000, 30000000, 100);

        public LA1JAntennaAnalyzerForm() {
            InitializeComponent();
            Controller = new Controller(this, new K6BEZ_DAQ(new DummyLineComm()));
            plotHandler = new PlotHandler();
        }

        private void Xaxis_AxisChanged(object sender, AxisChangedEventArgs e) {
            if (Controller != null) {
                int Start = (int)(plotHandler.XAxis.ActualMinimum * 1000000.0);
                int Stop = (int)(plotHandler.XAxis.ActualMaximum * 1000000.0);
                SetParameters(new SweepParameters(Start, Stop, sp.Steps));
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e) {
            Close();
        }

        private void Control_(bool value) {
            SweepButton.Enabled = value;
            BandComboBox.Enabled = value;
            StartFreqNumeric.Enabled = value;
            StopFreqNumeric.Enabled = value;
            StepsNumeric.Enabled = value;
            numericUpDown1.Enabled = value;
        }

        private void Control(bool value) {
            if (InvokeRequired) {
                BeginInvoke((MethodInvoker)delegate {
                    Control_(value);
                });
            } else {
                Control_(value);
            }
        }

        private void Sweep() {
            if (Controller == null) return;
            if (!Controller.Busy()) {
                SweepButton.Text = "Abort";
                int startfreq = (int)(StartFreqNumeric.Value * 1000000);
                int stopfreq = (int)(StopFreqNumeric.Value * 1000000);
                int steps = (int)StepsNumeric.Value;
                SetParameters(new SweepParameters(startfreq, stopfreq, steps));
                if (RepeatCheckbox.Checked) {
                    Controller.ManySweeps();
                } else {
                    Controller.Sweep();
                }
            } else {
                Controller.Stop();
            }
        }
        private void Button2_Click(object sender, EventArgs e) {
            Sweep();
        }

        private void BandComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            if (BandComboBox.Text == "160 m") {
                StartFreqNumeric.Value = 1.8M;
                StopFreqNumeric.Value = 2.0M;
            } else if (BandComboBox.Text == "80 m") {
                StartFreqNumeric.Value = 3.5M;
                StopFreqNumeric.Value = 4.0M;
            } else if (BandComboBox.Text == "60 m") {
                StartFreqNumeric.Value = 5.3M;
                StopFreqNumeric.Value = 5.5M;
            } else if (BandComboBox.Text == "40 m") {
                StartFreqNumeric.Value = 7.0M;
                StopFreqNumeric.Value = 7.3M;
            } else if (BandComboBox.Text == "30 m") {
                StartFreqNumeric.Value = 10.1M;
                StopFreqNumeric.Value = 10.2M;
            } else if (BandComboBox.Text == "20 m") {
                StartFreqNumeric.Value = 14.0M;
                StopFreqNumeric.Value = 14.4M;
            } else if (BandComboBox.Text == "17 m") {
                StartFreqNumeric.Value = 18.0M;
                StopFreqNumeric.Value = 18.2M;
            } else if (BandComboBox.Text == "15 m") {
                StartFreqNumeric.Value = 21.0M;
                StopFreqNumeric.Value = 21.5M;
            } else if (BandComboBox.Text == "12 m") {
                StartFreqNumeric.Value = 24.8M;
                StopFreqNumeric.Value = 25M;
            } else if (BandComboBox.Text == "10 m") {
                StartFreqNumeric.Value = 28.0M;
                StopFreqNumeric.Value = 29.7M;
            } else if (BandComboBox.Text == "6 m") {
                StartFreqNumeric.Value = 50.0M;
                StopFreqNumeric.Value = 54.0M;
            } else if (BandComboBox.Text == "HF") {
                StartFreqNumeric.Value = 3M;
                StopFreqNumeric.Value = 30M;
            } else if (BandComboBox.Text == "All") {
                StartFreqNumeric.Value = 0M;
                StopFreqNumeric.Value = 30M;
            }
        }

        private void SerialPortCombo_DropDown(object sender, EventArgs e) {
            SerialPortCombo.Items.Clear();
            SerialPortCombo.Items.Add("auto");
            SerialPortCombo.Items.Add("dummy");
            SerialPortCombo.Items.AddRange(SerialPort.GetPortNames());
        }

        private void SerialControl(bool value) {
            SerialPortCombo.Enabled = value;
            BaudRateCombo.Enabled = value;
            ProtocolCombo.Enabled = value;
        }

        void Connect() {
            SerialControl(false);
            ConnectButton.Text = "Disconnect";
            Control(true);
        }

        void Disconnect() {
            SerialControl(true);
            ConnectButton.Text = "Connect";
            if (Controller != null) Controller.Stop();
            Controller = null;
            Control(false);
        }

        private void ConnectButton_Click(object sender, EventArgs e) {
            if (!connected) {
                connected = true;
                configFile.Set("serial port", SerialPortCombo.Text);
                configFile.Set("baud rate", BaudRateCombo.Text);
                configFile.Set("protocol", ProtocolCombo.Text);
                // try {
                IDataAcquisition daq = Model.Factories.DAQFactory(SerialPortCombo.Text, int.Parse(BaudRateCombo.Text, CultureInfo.InvariantCulture), ProtocolCombo.Text);
                if (daq == null) {
                    throw new InvalidOperationException("Error: Cannot detect any devices. ");
                }
                Controller = new Controller(this, daq);

                if (!Controller.IsConnected()) {
                    throw new InvalidOperationException("Error: Cannot detect K6BEZ device on that serial port.");
                } else {
                    Sweep();
                    this.tabControl1.SelectedIndex++;
                }
                Connect();
                /*} catch (Exception ex) {
                    Controller = null;
                    MessageBox.Show(ex.Message);
                    Debug.WriteLine(ex.Message);
                }*/
            } else {
                connected = false;
                Disconnect();
            }

        }

        private void EksporterPNG(int w, int h) {
            if (Values != null) {

                double minswr = 1e9;
                double minfreq = -1;
                foreach (double[] meas in Values) {
                    if (meas[1] < minswr) {
                        minfreq = meas[0];
                        minswr = meas[1];

                    }
                }
                configFile.Set("PNG width", w.ToString(CultureInfo.InvariantCulture));
                configFile.Set("PNG height", h.ToString(CultureInfo.InvariantCulture));
                var filename = DateTime.Now.ToString("yyyy-MM-dd_HHmm", CultureInfo.InvariantCulture) + "_" + Values[0][0].ToString("0.0", CultureInfo.InvariantCulture) + "-" + Values[Values.Length - 1][0].ToString("0.0", CultureInfo.InvariantCulture) + "MHz_" + minswr.ToString("0.0", CultureInfo.InvariantCulture) + "SWR@" + minfreq.ToString("0.0", CultureInfo.InvariantCulture) + "MHz.png";
                plotHandler.ExportPNG(Settings.LogPath, filename, w, h);
                /*else {
                   MessageBox.Show("No data");
               }*/
            } else {
                MessageBox.Show("No data");
            }
        }

        private void SavePNGToolStripMenuItem_Click(object sender, EventArgs e) {
            EksporterPNG((int)PNGWidthNumeric.Value, (int)PNGHeightNumeric.Value);
        }

        private void MemoryButton_Click(object sender, EventArgs e) {
            plotHandler.Memory();
        }

        private void SaveCSVToolStripMenuItem_Click(object sender, EventArgs e) {
            double minswr = 1e9;
            double minfreq = 0;
            foreach (double[] meas in Values) {
                if (meas[1] < minswr) {
                    minfreq = meas[0];
                    minswr = meas[1];

                }
            }
            var initFileName = DateTime.Now.ToString("yyyy-MM-dd_HHmm", CultureInfo.InvariantCulture) + "_" + Values[0][0].ToString("0.0", CultureInfo.InvariantCulture) + "-" + Values[Values.Length - 1][0].ToString("0.0", CultureInfo.InvariantCulture) + "MHz_" + minswr.ToString("0.0", CultureInfo.InvariantCulture) + "SWR@" + minfreq.ToString("0.0", CultureInfo.InvariantCulture) + "MHz.tsv";
            var fileName = SaveFile(initFileName, "TSV file(*.tsv) | *.tsv | All files(*.*) | *.* ");
            if (fileName != null) {
                Misc.WriteCSV(fileName, new Measurements(GraphInfo, Values));
            }

        }

        private void NumericUpDown1_ValueChanged(object sender, EventArgs e) {

        }

        public void Write(string message) {
            try {
                BeginInvoke((MethodInvoker)delegate {
                    textBox2.AppendText(DateTime.Now.ToString(CultureInfo.InvariantCulture) + ": " + message);
                    textBox2.ScrollToCaret();
                });
            } catch {

            }
        }

        public void SetupView(GraphInfo[] graphInfo) {
            GraphInfo = graphInfo;
            plotHandler.SetupView(graphInfo);
            BeginInvoke((MethodInvoker)delegate {
                checkedListBox1.Items.Clear();
                for (int i = 1; i < graphInfo.Length; i++) {
                    checkedListBox1.Items.Add(graphInfo[i].Label);
                    checkedListBox1.SetItemChecked(i - 1, graphInfo[i].DefaultVisible);
                }
            });
        }

        public void PlotValues(IEnumerable<double[]> values) {
            Values = values.ToArray();
            this.BeginInvoke((MethodInvoker)delegate {
                plotHandler.PlotValues(Values);
            });
            FindMinSWR(Values);
        }

        private void FindMinSWR(double[][] values) {
            if (values.Length == 0) return;
            double minfreq = values[0][0];
            double minswr = values[0][1];
            int minidx = 0;

            int imax = 0;
            int imin = 0;
            int j = 0;

            foreach (double[] meas in values) {
                if (minswr > meas[1]) {
                    minfreq = meas[0];
                    minswr = meas[1];
                    minidx = j;
                }
                j++;
            }

            BeginInvoke((MethodInvoker)delegate {
                int i = 0;
                try {
                    i = minidx;
                    while (i < values.Length && values[i][1] < (double)numericUpDown1.Value) i++;
                    imax = i;

                    i = minidx;
                    while (i > 0 && values[i][1] < (double)numericUpDown1.Value) i--;
                    imin = i;
                    FreqLabel.Text = "fc: " + minfreq.ToString("0.00", CultureInfo.InvariantCulture) + " MHz\nBW: " + (1000.0 * values[imax][0] - 1000.0 * values[imin][0]).ToString("0", CultureInfo.InvariantCulture) + " kHz";
                } catch {

                }
            });
        }

        DateTime last = DateTime.Now;

        public void SetParameters(SweepParameters parameters) {
            if ((DateTime.Now - last).TotalSeconds > 0.1) {
                last = DateTime.Now;
                sp = parameters ?? throw new ArgumentNullException("parameters");

                decimal start = parameters.Start / 1000000M;
                decimal stop = parameters.Stop / 1000000M;
                decimal steps = parameters.Steps;

                if (start > StartFreqNumeric.Minimum && start < StartFreqNumeric.Maximum) StartFreqNumeric.Value = start;
                if (stop > StopFreqNumeric.Minimum && stop < StopFreqNumeric.Maximum) StopFreqNumeric.Value = stop;
                if (steps > StepsNumeric.Minimum && steps < StepsNumeric.Maximum) StepsNumeric.Value = steps;
                Controller.SweepParameters(sp);
            }
        }

        public void SweepDone() {
            BeginInvoke((MethodInvoker)delegate {
                SweepButton.Text = "Sweep";
                //Control(true);
            });
        }

        private void LA1JAntennaAnalyzer_FormClosing(object sender, FormClosingEventArgs e) {
            configFile.Save(Settings.LogPath);
            if (Controller != null) Controller.Stop();
        }

        private void RepeatCheckbox_CheckedChanged(object sender, EventArgs e) {
            if (Controller != null) {
                if (Controller.Busy() && (RepeatCheckbox.Checked == false)) Controller.Stop();
                //SetParameters(new SweepParameters(sp.Start, sp.Stop, sp.Steps));
            }
        }

        private void StartFreqNumeric_ValueChanged(object sender, EventArgs e) {
            /* if (Controller != null) {
                 SweepParameters p = Controller.SweepParameters;
                 p.Start = (int)(StartFreqNumeric.Value * 1000000M);
                 Controller.SweepParameters = p;
             }*/
        }

        private string OpenFile(string filter) {
            string ret = null;
            using (var ofd = new OpenFileDialog()) {
                ofd.InitialDirectory = Settings.LogPath;
                ofd.Filter = filter;
                if (ofd.ShowDialog() == DialogResult.OK) {
                    Controller.OpenTSV(ofd.FileName);
                    this.tabControl1.SelectedIndex = 1;
                }
            }
            return ret;
        }

        private static string SaveFile(string fileName, string filter) {
            string ret = null;
            using (var sfd = new SaveFileDialog()) {
                sfd.FileName = fileName;
                sfd.InitialDirectory = Settings.LogPath;
                sfd.Filter = filter;
                if (sfd.ShowDialog() == DialogResult.OK) {
                    ret = sfd.FileName;
                }
            }
            return ret;
        }

        private void OpenCSVToolStripMenuItem_Click(object sender, EventArgs e) {
            string fileName = OpenFile("TSV file(*.tsv)|*.tsv|All files(*.*)|*.*");
            if (fileName != null) {
                Controller.OpenTSV(fileName);
                this.tabControl1.SelectedIndex = 1;
            }
        }

        private void LA1JAntennaAnalyzer_Load(object sender, EventArgs e) {
            //mtl = new MyTraceListener(this);
            //Trace.Listeners.Add(mtl);

            configFile = new ConfigFile();
            configFile.Load(Settings.LogPath);
            tableLayoutPanel1.Controls.Add(plotHandler.GetView(), 1, 0);


            if (!String.IsNullOrEmpty(configFile.Get("serial port"))) {
                SerialPortCombo.Text = configFile.Get("serial port");
            }

            if (!String.IsNullOrEmpty(configFile.Get("baud rate"))) {
                BaudRateCombo.Text = configFile.Get("baud rate");
            }

            if (!String.IsNullOrEmpty(configFile.Get("protocol"))) {
                ProtocolCombo.Text = configFile.Get("protocol");
            }

            try {
                if (!String.IsNullOrEmpty(configFile.Get("PNG width"))) {
                    PNGWidthNumeric.Value = decimal.Parse(configFile.Get("PNG width"), CultureInfo.InvariantCulture);
                }
            } catch {

            }
            try {
                if (!String.IsNullOrEmpty(configFile.Get("PNG height"))) {
                    PNGHeightNumeric.Value = decimal.Parse(configFile.Get("PNG height"), CultureInfo.InvariantCulture);
                }
            } catch {

            }
            Control(false);

            plotHandler.XAxis.AxisChanged += Xaxis_AxisChanged;
            try {
                /*if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed) {
                    string version = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
                    //string version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    Text = "LA1J Antenna Analyzer by LA8DTA - " + version;*/
                //  } else {
                Text = "LA1J Antenna Analyzer by LA8DTA - DEVELOPMENT";
                //                }
            } catch (Exception ex) {
                Debug.WriteLine(ex.Message);
            }
        }

        private void CheckedListBox1_ItemCheck(object sender, ItemCheckEventArgs e) {
            plotHandler.SetSeriesVisibility(e.Index, (e.NewValue == CheckState.Checked));
        }

        public void ClearView() {
            plotHandler.ClearView();
        }

        private void CheckedListBox1_MouseClick(object sender, MouseEventArgs e) {
           
        }

        public void SetController(IController value) {
            this.Controller = value;
        }

        private void checkedListBox1_Click(object sender, EventArgs e) {
           
        }

        private void checkedListBox1_MouseDown(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Right) {
                CheckedListBox chlb = (CheckedListBox)sender;
                int indexFromPoint = chlb.IndexFromPoint(e.Location);
                plotHandler.ToggleAxis(indexFromPoint);
            }
        }

        private void StepsNumeric_ValueChanged(object sender, EventArgs e) {
            int steps = (int)StepsNumeric.Value;
            SetParameters(new SweepParameters(sp.Start, sp.Stop, steps));
        }

        private void ProtocolCombo_SelectedIndexChanged(object sender, EventArgs e) {

        }
    }
}
