﻿namespace Utilities

open System.IO.Ports
open System.Collections.Generic
open System
open System.Threading

type LineComm(serial,baud) =
    let sp = new SerialPort(serial, baud, Parity.None, 8, StopBits.One)
    do sp.RtsEnable <- true
       sp.DtrEnable <- true
       sp.ReadTimeout <- 1000
       sp.WriteTimeout <- 1000

    interface ILineComm with
        member this.Connect() =
            try 
                sp.Open()
                true
            with _ -> false
        member this.Disconnect() =
            if sp.IsOpen then sp.Close()
        member this.Read(arg1: int): Option<byte []> = 
            try
                if not sp.IsOpen then sp.Open()
                Some([| for i in 1 .. arg1 do yield byte(sp.ReadByte()) |])
            with | _ -> None
            
        member this.Read(): Option<string> =
            try 
                if not sp.IsOpen then sp.Open()
                let line = sp.ReadLine()
                Console.WriteLine("RX: " + line);
                Some line
            with _ -> None
            
        member this.ToString() = sp.PortName + " (" + sp.BaudRate.ToString() + ")";
        member this.Write(arg1:string) = 
            try 
                if not sp.IsOpen then sp.Open()
                sp.WriteLine(arg1)
                Console.WriteLine("TX: " + arg1);
            with _ -> ()
         member this.Write(arg1:byte[]) = 
            try 
                if not sp.IsOpen then sp.Open()
                sp.Write(arg1, 0, arg1.Length)
            with _ -> ()


type DummyLineComm() = // kan bygges om til MailboxProcessor ?
    let sweep = new Queue<string>()
    let mutable param = {Start=1000000; Stop=30000000; Steps=100}
    let rnd = new Random()
    let DoSweep() = 
        let df = float(param.Stop - param.Start) / float(param.Steps);
        [| for i in 1 .. (param.Steps-1) do 
            let freq = (float(param.Start) + float(i)*df)/1e6
            let swr = 1.0 + Math.Abs(200.0 - 200.0 / (1.0 + (freq - 14.07) * (freq - 14.07) * (freq - 10.13) * (freq - 10.13))) * (1.0 + rnd.NextDouble() / 10.0) + rnd.NextDouble() / 10.0
            yield [| freq; swr |] |]
    interface ILineComm with
        member this.Write(arg1:byte[]) = 
            ()
        member this.Disconnect() = ()
        member this.Read(): Option<string> =
            Thread.Sleep(10);
            if sweep.Count <> 0 then Some(sweep.Dequeue()) else None
        member this.Read(arg1: int): Option<byte []> = None
        member this.ToString() = "dummy"
        member this.Write(arg1:string) = 
            match arg1 with
            | s when arg1.Contains("A") -> param <- {param with Start = int(s.Substring(0, s.Length - 1))}
            | s when arg1.Contains("B") -> param <- {param with Stop = int(s.Substring(0, s.Length - 1))}
            | s when arg1.Contains("N") -> param <- {param with Steps = int(s.Substring(0, s.Length - 1))}
            | s when arg1.Contains("?") -> sweep.Enqueue("Num Steps" + Environment.NewLine)
            | s when arg1.Contains("S") || arg1.Contains("s") -> 
                let s = DoSweep()
                let f = 100000.0
                s |> Array.iter (fun arr -> 
                    let swr = arr.[1]
                    let r = (f * swr - f)/(swr + 1.0)
                    let sfreq = int(1e6*arr.[0]).ToString()
                    let sswr = int(1000.0*swr).ToString()
                    let line = sfreq + "," + sswr + "," + f.ToString("0") + "," + int(r).ToString()
                    //printfn "%s" line
                    sweep.Enqueue(line + Environment.NewLine))
            | _ -> ()
        member this.Connect() = true