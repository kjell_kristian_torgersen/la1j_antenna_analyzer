﻿namespace Model
open Utilities
open System.Diagnostics
open System
open System.Threading
open System.IO.Ports
open Microsoft.FSharp.Core
open System.Collections.Concurrent
open System.Threading.Tasks
open Utilities.Misc
open System.IO

type K6BEZ_DAQ_Binary(comm : ILineComm) =
    let mutable mparam = {Start=1000000;Stop=30000000;Steps=100}
    interface IDataAcquisition with
        member this.Connect() = comm.Connect()
        member this.Disconnect() = comm.Disconnect()
        member this.IsConnected() = 
            (*Debug.WriteLine("IsConnected() called ...")
            let task = async { 
                for i in 1..10 do
                    comm.Write("?")
                    Debug.WriteLine('?')
                    Thread.Sleep(1000)
                }
            Async.Start(task)
            let rec loop(line:Option<string>) =
                match line with
                | Some line -> if line.StartsWith("Num Steps") then true else loop (comm.Read())
                | None -> false
            loop(comm.Read())*)
            true

        member this.SetParameters(param) =
            mparam <- param
            let startFreq = System.Math.Max(mparam.Start, 0)
            let fdiff = (mparam.Stop - mparam.Start) / mparam.Steps
            comm.Write([| 0x55uy|])
            comm.Write(BitConverter.GetBytes(startFreq))
            comm.Write(BitConverter.GetBytes(fdiff))
            comm.Write(BitConverter.GetBytes(mparam.Steps))
        member this.Sweep() =
            comm.Write([| (byte)'X' |])      
            let data = comm.Read(4*mparam.Steps)
           
            let fdiff = (mparam.Stop - mparam.Start) / mparam.Steps
            let startFreq = System.Math.Max(mparam.Start, 0)
            match data with
            | Some data ->
                File.WriteAllBytes("debug.bin", data)
                let wdata = byteToWordArray data
                seq {
                    for i in 0 .. (mparam.Steps-1) do
                        let freq = float(i*fdiff + startFreq)/1e6;
                        let fwd = float (wdata.[2*i])
                        let rev = float (wdata.[2*i+1])
                        let swr = (fwd+rev)/(fwd-rev)
                        yield [| freq;swr;fwd;rev|]
                }
            | None -> Seq.empty
            
        member this.GraphInfo = [|
            {Label="Frequency [MHz]"; Color={R=0;G=0;B=0};Secondary=false;DefaultVisible=true}
            {Label="SWR"; Color={R=0;G=0;B=192};Secondary=false;DefaultVisible=true}
            {Label="Forward"; Color={R=128;G=128;B=0};Secondary=true;DefaultVisible=false}
            {Label="Reverse"; Color={R=0;G=128;B=128};Secondary=true;DefaultVisible=false}
        |]

type K6BEZ_DAQ(comm : ILineComm) =
    interface IDataAcquisition with
        member this.Connect() = comm.Connect()
        member this.Disconnect() = comm.Disconnect()
        member this.IsConnected() = 
            Debug.WriteLine("IsConnected() called ...")
            (*let cts = new CancellationTokenSource()
            let task = async { 
                for i in 1..10 do
                    comm.Write("?")
                    Debug.WriteLine('?')
                    Thread.Sleep(100)
                }
            Async.Start(task, cts.Token)*)
            let rec loop(line:Option<string>) =
                match line with
                | Some line -> if line.StartsWith("Num Steps") then true else loop (comm.Read())
                | None -> false
            let result = loop(comm.Read())
            //cts.Cancel()
            result

        member this.SetParameters(param) =
            let startFreq = System.Math.Max(param.Start, 0)
            comm.Write(startFreq.ToString() + "A")
            comm.Write(param.Stop.ToString() + "B")
            comm.Write(param.Steps.ToString() + "N")

        member this.Sweep() = 
            comm.Write("S")
            let rec loop(line:Option<string>) = seq {
                match line with
                | None -> ()
                | Some line -> if not (line.Contains("End")) then
                                let split = line.Split(',')
                                if split.Length = 4 then
                                    let tryfloat x = try float x with | _ -> Double.NaN
                                    let floats = Array.map tryfloat split
                                    let frq = floats.[0]/1e6
                                    let fwd = floats.[2]
                                    let rev = floats.[3]
                                    let swr = (fwd+rev)/(fwd-rev)
                                    yield [| frq; swr; fwd;  rev |] 
                                    yield! loop(comm.Read())
            }
            let ret = loop(comm.Read())
            ret
        member this.GraphInfo = [|
            {Label="Frequency [MHz]"; Color={R=0;G=0;B=0};Secondary=false;DefaultVisible=true}
            {Label="SWR"; Color={R=0;G=0;B=192};Secondary=false;DefaultVisible=true}
            {Label="Forward"; Color={R=128;G=128;B=0};Secondary=true;DefaultVisible=false}
            {Label="Reverse"; Color={R=0;G=128;B=128};Secondary=true;DefaultVisible=false}
        |]

type miniVNA_DAQ(comm : ILineComm) =
    let mutable mparams = {Start=1000000;Stop=30000000;Steps=100}
    member this.comm = comm
    override this.ToString()  = "miniVNA @ " + comm.ToString()
    interface IDataAcquisition with
        member this.Connect() = comm.Connect()
        member this.Disconnect() = comm.Disconnect()
        member this.IsConnected() = true
        member this.SetParameters(param) =
            mparams <- param
        member this.Sweep() = 
            let DDSRATIO = 4294967296.0 / 400000000.0;
            let stepSize = float(mparams.Stop - mparams.Start) / float(mparams.Steps);
            comm.Write("0\r");
            comm.Write((float(mparams.Start) * DDSRATIO).ToString("0") + "\r");
            comm.Write(mparams.Steps.ToString() + "\r");
            comm.Write((stepSize * DDSRATIO).ToString("0") + "\r");
            let bytes = comm.Read(4*mparams.Steps)
            match bytes with
            | Some bytes ->
                seq {
                    for i in 0..(mparams.Steps-1) do
                        let freq = float(mparams.Start) + float(i) * stepSize
                        let phi_raw = float(BitConverter.ToUInt16(bytes, 4*i))
                        let return_loss_raw = float(BitConverter.ToUInt16(bytes, 4 * i + 2))
                        let return_loss_db = -return_loss_raw * 60.0 / 1024.0
                        let phi_deg = phi_raw * 180.0 / 1024.0
                        let mag = Math.Pow(10.0, return_loss_db / 20.0)
                        let swr = (1.0+mag)/(1.0-mag)
                        yield [| freq/1e6; swr; phi_raw; return_loss_raw; phi_deg; return_loss_db; mag|]
                }
            | None -> Seq.empty

        member this.GraphInfo = [|
            {Label="Frequency [MHz]"; Color={R=0;G=0;B=0};Secondary=false;DefaultVisible=true}
            {Label="SWR"; Color={R=0;G=0;B=192};Secondary=false;DefaultVisible=true}
            {Label="Phi raw"; Color={R=128;G=128;B=128};Secondary=true;DefaultVisible=false}
            {Label="Return loss raw"; Color={R=192;G=0;B=0};Secondary=true;DefaultVisible=false}
            {Label="Phi"; Color={R=192;G=192;B=0};Secondary=false;DefaultVisible=true}
            {Label="Return loss [dB]"; Color={R=0;G=192;B=0};Secondary=false;DefaultVisible=true}
            {Label="gamma"; Color={R=0;G=192;B=192};Secondary=false;DefaultVisible=true}
        |]

type SerialScanner() =
    let mutable _done = false
    let blockingCollection = new BlockingCollection<IDataAcquisition>()
    member this.Abort() = _done <- true

    member this.ScanThread(lineComm:Object) =
        let lineComm = lineComm :?> LineComm
        let daq = new K6BEZ_DAQ(lineComm) :> IDataAcquisition
        daq.Connect() |> ignore
        while not _done do
            if daq.IsConnected() then
                blockingCollection.Add(daq)

    member this.ScanAll(baudrate:int) =
        _done <- false
        SerialPort.GetPortNames() |> Array.iter (
            fun port -> 
                Debug.WriteLine("Scanning " + port + " in its own thread ... ")
                let th2 = new Thread(new ParameterizedThreadStart(this.ScanThread), Name = port + " scanner thread")
                th2.Start(new LineComm(port, baudrate)) |> ignore
        )
        let ret = blockingCollection.Take()
        _done <- true
        Some(ret)

type AutoDAQ(baudrate:int) =
    let mutable mparam = {Start=1000000;Stop=30000000;Steps=100}
    let baudrate = baudrate
    let ss = new SerialScanner()
    let mutable daq : Option<IDataAcquisition> = None
    interface IDataAcquisition with
        member this.Disconnect() = ss.Abort()

        member this.GraphInfo =
            match daq with
                | None -> [| |]
                | Some dq -> dq.GraphInfo

        member this.IsConnected() = true

        member this.SetParameters(param) =
            mparam <- param

        member this.Sweep()  =
            match daq with
            | None -> daq <- ss.ScanAll(baudrate)
            | _ -> ()
            match daq with
            | Some daq -> daq.Sweep() 
            | None -> Seq.empty

        member this.Connect() = true

module Factories =
    let DAQFactory (serial,baudrate,protocol) : IDataAcquisition =
        match (serial,protocol) with
        | ("dummy",_) -> new K6BEZ_DAQ(new DummyLineComm()) :> IDataAcquisition
        | ("auto",_) -> new AutoDAQ(baudrate) :> IDataAcquisition
        | (_,"K6BEZ") -> new K6BEZ_DAQ(new LineComm(serial, baudrate)) :> IDataAcquisition
        | (_,"miniVNA") -> new miniVNA_DAQ(new LineComm(serial, baudrate)) :> IDataAcquisition
        | (_,"Binary") -> new K6BEZ_DAQ_Binary(new LineComm(serial, baudrate)) :> IDataAcquisition
        | (_,_) -> new AutoDAQ(baudrate) :> IDataAcquisition