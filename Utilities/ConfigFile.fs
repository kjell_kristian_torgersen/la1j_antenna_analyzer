﻿namespace Utilities
open System.Collections.Generic
open System.IO
open System

type ConfigFile() =
    let map = new Dictionary<string,string>()
    member this.Load(logpath:string) =
        //try 
            Directory.CreateDirectory(logpath) |> ignore
            let filename = Path.Combine(logpath, "configuration.txt")
            let lines = File.ReadAllLines(filename)
            lines |> Array.iter (fun line ->
                let kv = line.Split([| ": " |], StringSplitOptions.None)
                map.Add(kv.[0].ToUpperInvariant(), kv.[1])
            )
        //with _ -> ()
    member this.Save(logpath:string) =
        let filename = Path.Combine(logpath, "configuration.txt")
        use file = new System.IO.StreamWriter(filename, false)
        for kv in map do 
            let line = kv.Key + ": " + kv.Value
            Console.WriteLine(line)
            file.WriteLine(line)
        file.Flush()
        file.Close()
        ()
    member this.GetO(key:string) =
        let key = key.ToUpperInvariant()
        if map.ContainsKey(key) then Some map.[key]
        else None
    member this.Get(key:string) =
        let key = key.ToUpperInvariant()
        if map.ContainsKey(key) then map.[key]
        else ""

    member this.Set(key:string,value:string) = 
        let key = key.ToUpperInvariant()
        if map.ContainsKey(key) then
            map.[key] <- value
        else
            map.Add(key, value) |> ignore