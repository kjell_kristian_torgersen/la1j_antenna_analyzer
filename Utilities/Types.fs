﻿namespace Utilities

type Color = {R:int;G:int;B:int}
type GraphInfo = {Label:string;Color:Color;Secondary:bool;DefaultVisible:bool}
type Measurements = {GraphInfo:GraphInfo[]; Values:float[]seq}
type SweepParameters = {Start : int; Stop:int; Steps:int}
type SweepInfo = {CenterFreq :float; SWR:float; BandWidth:float}

type ILineComm =
    abstract member Connect : unit -> bool
    abstract member Disconnect : unit -> unit
    abstract member Read : unit -> Option<string>
    abstract member Write : string -> unit
    abstract member Write : byte[] -> unit
    abstract member Read : int -> Option<byte[]>
    abstract member ToString : unit -> string

type IDataAcquisition =
    abstract member GraphInfo : GraphInfo[] with get
    abstract member Connect : unit -> bool
    abstract member IsConnected : unit -> bool
    abstract member SetParameters : SweepParameters -> unit
    abstract member Sweep : unit -> float[] seq
    abstract member Disconnect : unit -> unit

type IController =
    abstract member SweepParameters : SweepParameters -> unit
    abstract member Busy : unit -> bool
    abstract member IsConnected : unit -> bool
    abstract member Stop : unit -> unit
    abstract member Disconnect : unit -> unit
    abstract member Sweep : unit -> bool
    abstract member ManySweeps : unit -> bool
    abstract member OpenTSV : string -> unit

type IView =
    abstract member ClearView : unit -> unit
    abstract member SetupView : GraphInfo[] -> unit
    abstract member PlotValues : float[]seq -> unit
    abstract member SweepDone : unit -> unit
    abstract member SetController : IController -> unit
