﻿namespace Utilities

open Misc

type SweepCommands =
    | IsBusy of AsyncReplyChannel<bool>
    | SingleSweep
    | StartManySweeps
    | ManySweeps
    | StopManySweeps
    | SetParameters of SweepParameters

type Controller(view:IView,device:IDataAcquisition) =
    let rec sweepAgent = MailboxProcessor<SweepCommands>.Start((fun inbox ->
        let rec messageLoop(repeatSweep) = async {
            let sweep() =
                let swr = device.Sweep() |> Seq.toArray
                view.ClearView()
                view.SetupView(device.GraphInfo)
                view.PlotValues(swr)

            let singleSweep() =
                sweep()
                view.SweepDone()

            let manySweeps() =
                if repeatSweep then
                    let swr = device.Sweep() |> Seq.toArray
                    view.ClearView()
                    view.PlotValues(swr)
                    sweepAgent.Post(ManySweeps)
                else
                    view.SweepDone()
                    device.Disconnect()

            let! msg = inbox.Receive()
            printfn "msg = %A" msg
            match msg with
            | SingleSweep -> singleSweep()
            | StartManySweeps -> 
                sweep()
                sweepAgent.Post(ManySweeps) |> ignore
                return! messageLoop(true)
            | ManySweeps -> manySweeps()
            | StopManySweeps -> return! messageLoop(false)
            | SetParameters newParam ->
                device.SetParameters newParam
            | IsBusy replyChannel -> replyChannel.Reply(repeatSweep)
            return! messageLoop(repeatSweep)
        }
        messageLoop(false)))

    interface IController with 
        member this.Busy() = 
            sweepAgent.PostAndReply(fun replyChannel -> IsBusy replyChannel)

        member this.Stop() =
            sweepAgent.Post(StopManySweeps) |> ignore
        member this.Disconnect() =
            device.Disconnect()

        member this.OpenTSV(fileName) =
            (this :> IController).Stop()
            let meas = Misc.ReadCSV(fileName)
            view.ClearView()
            view.SetupView(meas.GraphInfo)
            view.PlotValues(meas.Values)
        
        member this.Sweep() =
            device.Connect() |> ignore
            sweepAgent.Post(SingleSweep) |> ignore
            true
        member this.ManySweeps() =
            device.Connect() |> ignore
            sweepAgent.Post(StartManySweeps) |> ignore
            true

        member this.SweepParameters(param) =
            sweepAgent.Post(SetParameters param)

        member this.IsConnected() = true