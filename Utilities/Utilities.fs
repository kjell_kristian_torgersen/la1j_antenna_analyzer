﻿namespace Utilities
open OxyPlot
open System
open System.IO
open System.Threading
open System.IO.Ports
open System.Diagnostics

    module Misc =

        let dummyLineComm() = 
            let mp = MailboxProcessor.Start((fun inbox ->
                let rec messageLoop() = async {
                    let! msg = inbox.Receive()
                    printfn "%A" msg
                    return! messageLoop()
                }
                messageLoop()))
            mp
        let byteToWordArray (arr:byte[]) =
            [| for i in 0..(arr.Length/2-1) do yield BitConverter.ToUInt16(arr, 2*i) |]
        let rec processLines (lines:string seq) = seq {
            let lineIsNotEnd (line:string) = not (line.Contains("End"))
            let lineToFloatArr (line:string) = Array.map float (line.Split(','))
            let output = lines |> Seq.takeWhile lineIsNotEnd |> Seq.map lineToFloatArr |> Seq.toArray
            let rest =  (Seq.skipWhile lineIsNotEnd lines)
            yield output; if not (Seq.isEmpty rest) then yield! processLines (Seq.skip 1 rest)
        }
        let ReadCSV (fileName:string) =
            let rng = System.Random() // random number generator for making random colors
            let randByte() = rng.Next() % 192 // make a random byte for color
            let randColor() = {R=randByte(); G=randByte(); B=randByte()} // make random color
            use sr = new StreamReader (fileName) // open file
            let labels = sr.ReadLine().Split('\t') // split the first line to get labels
            let graphInfos = [| for label in labels -> {Label=label;Color=randColor();Secondary=false;DefaultVisible=true}|]
            let values = [|while not sr.EndOfStream do // as long as there is more lines
                            let line = sr.ReadLine().Replace(',','.')
                            let split = line.Split('\t') // read and split a line
                            Array.iter (printf "%s ") split
                            printfn ""
                            yield Array.map float split |] // convert each splitted line to an double array
            {GraphInfo = graphInfos; Values=values} // return new GraphInfo object
        
        let WriteCSV (fileName:string, measurements:Measurements) =
            use sw = new StreamWriter(fileName)
            let labels = [| for gi in measurements.GraphInfo -> gi.Label |]
            sw.WriteLine(String.Join("\t",labels))
            for values in measurements.Values do
                 sw.WriteLine(String.Join("\t",(values |> Array.map (fun x -> x.ToString(System.Globalization.CultureInfo.InvariantCulture)))))           

        let DummyMeasurement =
            let gi1 = {Label="Time"; Color={R=0;G=0;B=0}; Secondary=false; DefaultVisible=true}
            let gi2 = {Label="SWR"; Color={R=0;B=0;G=192}; Secondary=false; DefaultVisible=true}
            let values = [|[|0.0;1.0|];[|1.0;2.0|];[|2.0;4.0|]|]
            {GraphInfo=[|gi1;gi2|]; Values=values}
        
        let ExtractSweepInfo (values:float[]seq) =
            let minSWR = values |> Seq.fold (fun (idx,freq,swr,ctr) b -> if swr < b.[1] then (idx,freq,swr,ctr+1) else (ctr,b.[0],b.[1],ctr+1)) (-1,0.0,Double.MaxValue,0)
            minSWR
    module Settings =
        let path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        let mutable LogPath = Path.Combine(path, "LA1J_Antenna_Analyzer");