﻿namespace Utilities
open OxyPlot
open OxyPlot.Axes
open OxyPlot.Series
open OxyPlot.WindowsForms
open System.Windows.Forms

type PlotHandler() =
    let myModel = new PlotModel()
    let plot1 = new PlotView(Dock = DockStyle.Fill, BackColor=System.Drawing.Color.White, Model=myModel)
    let xaxis = new LinearAxis(Title="Frequency [MHz]",Position = AxisPosition.Bottom, MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot)
    let yaxis = new LinearAxis(Title="Primary value",Position = AxisPosition.Left, MajorGridlineStyle = LineStyle.Solid, MinorGridlineStyle = LineStyle.Dot, Key = "primary")
    let yaxis2 = new LinearAxis(Title="Secondary value", Position = AxisPosition.Right, Key = "secondary" )
    let ToOxyColor(c) = OxyColor.FromRgb(byte c.R, byte c.G, byte c.B)
    let mutable memory : LineSeries = new LineSeries( Title = "Memory", StrokeThickness = 4.0, Color = ToOxyColor{R=128;G=0;B=0}, IsVisible = true )
    let mutable limitser : LineSeries = new LineSeries( Title = "Limit", StrokeThickness = 4.0, Color = ToOxyColor{R=128;G=128;B=128}, IsVisible = true )

    do  myModel.Axes.Add(xaxis)
        myModel.Axes.Add(yaxis)
        myModel.Axes.Add(yaxis2)
    member this.XAxis = xaxis

    member this.ExportPNG(logPath:string, fileName:string, w:int, h:int) =
       let pngExporter = new PngExporter(Width = w, Height = h, Background = OxyColors.White)
       use sfd = new SaveFileDialog(InitialDirectory = logPath, FileName = fileName, Filter = "PNG file(*.png)|*.png|All files(*.*)|*.*")
       if sfd.ShowDialog() = DialogResult.OK then pngExporter.ExportToFile(myModel, sfd.FileName)

    member this.PlotValues (values:double[]seq) =
        printfn "PlotValues"
        values |> Seq.iteri (fun j meas -> 
            for i in 1 .. (meas.Length-1) do
                let ser = myModel.Series.[i - 1] :?> LineSeries
                ser.Points.Add(new DataPoint(meas.[0], meas.[i])))
        myModel.InvalidatePlot(true)
        
    member this.ClearView() =
        printfn "ClearView"
        myModel.Series |> Seq.map ( fun u -> u :?> LineSeries) |> Seq.iter (fun u -> if u <> memory && u <> limitser then u.Points.Clear())

    member this.SetupView(graphInfo:GraphInfo[]) =
        myModel.Series.Clear()
        graphInfo |> Array.tail |> Array.iter (fun g -> 
            if g.Secondary then
                myModel.Series.Add(new LineSeries( Title = g.Label, StrokeThickness = 4.0, YAxisKey = "secondary", Color = ToOxyColor(g.Color), IsVisible = g.DefaultVisible))
            else
                myModel.Series.Add(new LineSeries( Title = g.Label, StrokeThickness = 4.0, Color = ToOxyColor(g.Color), IsVisible = g.DefaultVisible )))
        myModel.Series.Add(memory)
        myModel.Series.Add(limitser)

    member this.GetView() = plot1

    member this.SetSeriesVisibility(index, visible) =
        if index < myModel.Series.Count then
            myModel.Series.[index].IsVisible <- visible
            myModel.InvalidatePlot(true)

    member this.ToggleAxis(index) =
        if index < myModel.Series.Count then
            let ser = myModel.Series.[index] :?> LineSeries
            if ser.YAxisKey = "secondary" then 
                ser.YAxisKey <- "primary" 
                ser.LineStyle <- LineStyle.Solid
            else 
                ser.YAxisKey <- "secondary"
                ser.LineStyle <- LineStyle.Dash
            myModel.InvalidatePlot(true)
    member this.Memory() =
        if memory.Points.Count <> 0 then
            memory.Points.Clear()
        else
            let ser = myModel.Series.[0] :?> LineSeries
            for p in ser.Points do memory.Points.Add(p)
        plot1.InvalidatePlot(true)

    member this.SetLimit(limit:float) =
        limitser.Points.Clear()
        let ser = myModel.Series.[0] :?> LineSeries
        for p in ser.Points do memory.Points.Add(new DataPoint(p.X, limit))
        plot1.InvalidatePlot(true)

    member this.Refresh() = myModel.InvalidatePlot(true)